function fahrenheitToCelcius(temp) {
    return ((temp-32)*5/9).toFixed(3);
}

function celciusToFahrenheit(temp) {
    return ((temp*9/5)+32).toFixed(3);
}

function milesToKilometers(miles) {
    return (miles*1.6).toFixed(3);
}

function ouncesToGrams(ounces) {
    return (ounces*28.35).toFixed(3);
}

function isValid(val, floor) {
    return val >= floor;
}
