"use strict";
$(document).ready(() => 
    $("#jdom a").click(function (evt) { 
        $(this).prev().toggleClass("hide");
        if($(this).attr("class") !== "hide")
            $(this).text("Show less");
        else
            $(this).text("Show less");
        evt.preventDefault();
    })
);
