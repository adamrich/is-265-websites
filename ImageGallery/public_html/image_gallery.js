"use strict";
$(document).ready(() => {
    $("#image_list a").each(() => {
        $('<img />').attr('src', $(this).attr("href")).attr('alt', $(this).attr("title"));
    });


    $("#image_list a").click(function (evt) { 
        $("#image").attr("src", $(this).attr("href"));
        $("#image").attr("alt", $(this).attr("title"));
        $("#caption").text($(this).attr("title"));
        evt.preventDefault();
    })
    $("#image_list").find("a:first").focus();
});
