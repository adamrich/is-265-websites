var $ = function(id) {
    return document.getElementById(id);
};

window.onload = function() {
    $("btnClear").onclick = clrscrn;
    $("btnConvert").onclick = convert;
    $("txtIn").focus();
    $("txtOut").value = "";
    $("txtCalc").value = "";
};

function clrscrn() {
    $("txtIn").value = "";
    $("txtCalc").value = "";
    $("txtOut").value = "";
    $("textin").focus();
}

function convert() {
    let val = $("txtIn").value
    let dVal = parseFloat(val);
    $("txtOut").value = "";
    $("txtCalc").value = "";

    if(val === "") {
        alert("No value entered for conversion.")
        return;
    } else if(!(/^\d+$/.test(val))) { //for some reason, isNaN(val) fails for the string '1e1'
                                      //but '1a1' is recognised as being not a number.
                                      //I guess that I found an edge case.
        alert("Input value is not a number.");
        return;
    } else if(val <= 0) {
        alert("Input value must be a positive integer.");
        return;
    } else if(!Number.isInteger(dVal)) {
        alert("Input value is not an integer.");
        return;
    }

    let cT = document.getElementsByName("tType");
    if(cT[0].checked) {
        decToBinArr(val);
    } else if(cT[1].checked) {
        if(!(/^[0-1]+$/.test(val))) {
            alert("Input value must contain only zeros and ones.");
            return;
        }
        binToDec(val);
    } else {
        alert("Please select a conversion type.");
        return;
    }
}

function decToBinArr(val) {
    let remainders = [];
    let curr = val;

    while(curr > 0) {
        let rem = curr%2;
        let next = Math.floor(curr/2);
        $("txtCalc").value = $("txtCalc").value + curr + " divided by 2 = " + next + " w/remainder of: " + rem + '\n';
        remainders.push(rem);
        curr = next;
    }

    for(let i=remainders.length-1; i>=0; i--) {
        $("txtOut").value = $("txtOut").value + remainders[i];
    }
}

function binToDec(val) {
    let curr = val;
    let currPow = 0;
    let sum = 0;

    while(curr > 0) {
        let place = curr%10 * Math.pow(2,currPow);
        let next = Math.floor(curr/10);
        $("txtCalc").value = $("txtCalc").value + "There is a " + place + " in the value (2^" + currPow + ")\n";
        currPow++;
        sum += place;
        curr = next;
    }

    $("txtOut").value = sum;
}
