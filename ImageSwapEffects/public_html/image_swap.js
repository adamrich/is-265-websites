$(document).ready(function() {
	// preload images
	$("#image_list a").each(function() {
		var swappedImage = new Image();
		swappedImage.src = $(this).attr("href");
	});
	
	// set up event handlers for links    
	$("#image_list a").click(function(evt) {
        var imageURL = $(this).attr("href");
        var caption = $(this).attr("title");
        let image = $("#image");

        image.animate({"opacity": 0}, {duration: 1000, done: function() {
            image.attr("src", imageURL);
            image.animate({"opacity": 1}, 1000);
        }});
        $("#caption").animate({"opacity": 0}, {duration: 1000, complete: function() {
            $("#caption").text(caption);
            $("#caption").animate({"opacity": 1}, 1000);
        }});

		// cancel the default action of the link
	    evt.preventDefault();
	}); // end click
	
	// move focus to first thumbnail
	$("li:first-child a").focus();
}); // end ready
