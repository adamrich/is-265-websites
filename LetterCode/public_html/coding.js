var $ = function(id) {
    return document.getElementById(id);
};

function clrscrn() {
    $("textin").value = "";
    $("textout").value = "";
    $("textin").focus();
    $("textin_span").innerHTML = "";
}

window.onload = function() {
    $("btnClear").onclick = clrscrn;
    $("btnEncode").onclick = encode;
    $("btnDecode").onclick = decode;
    $("textin").focus();
};

window.onerror = function(a, b, c, d) {
    alert(a + '\n' + b + '\n' + c + '\n' + d);
    return false;
};

function encode() {
    let encode_str = $("textin").value.toUpperCase();
    
    if(encode_str === '') {
        $("textin_span").innerHTML = "* Please enter a message to encode *";
        $("textin").focus();
        return;
    } else {
        $("textin_span").innerHTML = "";
    }

    $("textin").value = encode_str;
    encode_str = encode_str.split('');

    for(let i=0; i<encode_str.length; i++) {
        let currVal = encode_str[i].charCodeAt(0);

        if(encode_str[i] === ' ') {
            encode_str[i] = 0;
        } else if(currVal >= 65 && currVal <= 90) {
            encode_str[i] = encode_str[i].charCodeAt(0) - 64;
        } else {
            encode_str[i] = 99;
        }
    }

    $("textout").value = encode_str.join(' ');
    return;
}

function decode() {
    let decode_str = $("textin").value;

    if(decode_str === '') {
        $("textin_span").innerHTML = "* Please enter a message to decode *";
        $("textin").focus();
        return;
    } else {
        $("textin_span").innerHTML = "";
    }

    decode_str = decode_str.split(',');

    for(let i=0; i<decode_str.length; i++) {
        let currVal = Number.parseInt(decode_str[i].trim());

        if(isNaN(decode_str[i].trim())) {
            decode_str[i] = '?';
        } else if(currVal === 0) {
            decode_str[i] = ' ';
        } else if(currVal >= 1 && currVal <= 26) {
            decode_str[i] = String.fromCharCode(currVal + 64);
        } else {
            decode_str[i] = '?';
        }
    }

    $("textout").value = decode_str.join('');
    return;
}
