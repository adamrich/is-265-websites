var $ = function(id) {
    return document.getElementById(id);
};

function clrscrn() {
    $("form1").reset();
    $("results").innerHTML = "";
    $("StudentNo").focus();
}

window.onload = function() {
    $("btnClr").onclick = clrscrn;
    $("btnCalc").onclick = calculate;
    $("StudentNo").focus();
};

window.onerror = function(a, b, c, d) {
    alert(a + '\n' + b + '\n' + c + '\n' + d);
    return false;
};

function calculate() {
    let sno = validateSNo();
    if(sno === "") {
        return;
    }

    let lnm = $("LastName").value.trim();
    if(lnm === "") {
        alert("Last Name is missing.")
        $("LastName").focus();
        return;
    }

    let fnm = $("FirstName").value.trim();
    if(fnm === "") {
        alert("First Name is missing.")
        $("FirstName").focus();
        return;
    }

    let q1 = validateScore("Q1");
    if(q1 === -1) {
        return;
    }

    let q2 = validateScore("Q2");
    if(q2 === -1) {
        return;
    }

    let q3 = validateScore("Q3");
    if(q3 === -1) {
        return;
    }

    let q4 = validateScore("Q4");
    if(q4 === -1) {
        return;
    }

    let q5 = validateScore("Q5");
    if(q5 === -1) {
        return;
    }

    let qm;
    if($("QM").value === "") {
        qm = 0;
    } else {
        qm = validateScore("QM");
        if(qm === -1) {
            return;
        }
    }

    let qavg = quizAverage(q1, q2, q3, q4, q5, qm);

    let mt = validateScore("MidTerm");
    if(mt === -1) {
        return;
    }

    let pr = validateScore("Problems");
    if(pr === -1) {
        return;
    }

    let cavg, lGrade;
    if(qavg >= 90 && mt >= 90 && pr >= 90) {
        cavg = (qavg + mt + pr)/3;
        lGrade = 'A';
    } else {
        let fe = validateScore("Final");
        if(fe === -1) {
            return;
        }

        cavg = (qavg * .5) + (mt * .15) + (pr * .1) + (fe * .25);
        
        if(cavg >= 89.5) {
            lGrade = 'A';
        } else if(cavg >= 79.5) {
            lGrade = 'B';
        } else if(cavg >= 69.5) {
            lGrade = 'C';
        } else if(cavg >= 59.5) {
            lGrade = 'D';
        } else {
            lGrade = 'F';
        }
    }

    $("results").innerHTML = "Quiz Average = " + qavg.toFixed(1) +
                             " and course average = " + cavg.toFixed(2) +
                             " for a Grade of: " + lGrade;
}

function validateScore(id) {
    let s = parseFloat($(id).value);
    if(isNaN(s) || s < 0 || s > 125) {
        alert(id + " must be a number from 0 to 125");
        $(id).focus();
        s = -1;
    }
    return s;
}

function validateSNo() {
    let sno = $("StudentNo").value.trim();
    
    if(sno === "") {
        alert("Student Number is missing.")
        $("StudentNo").focus();
        sno = "";
    } else if(sno[0] !== 'A') {
        alert("Student Number must start with an A.");
        $("StudentNo").focus();
        sno = "";
    } else if(sno.length > 9) {
        alert("Student Number is too long.")
        $("StudentNo").focus();
        sno = "";
    } else if(sno.length < 9) {
        alert("Student Number is too short.")
        $("StudentNo").focus();
        sno = "";
    } else if(isNaN(sno.substring(1))) {
        alert("Student Number should be in the format of an A followed by 8 digits.");
        $("StudentNo").focus();
        sno = "";
    } else if(sno.substring(1) == '00000000') {
        alert("The digits in the Student Number should not all be 0.");
        $("StudentNo").focus();
        sno = "";
    }

    return sno;
}

function quizAverage(q1, q2, q3, q4, q5, qm) {
    let qa = [q1, q2, q3, q4, q5, qm];
    qa.sort((a, b) => a-b);
    return (qa[2]+qa[3]+qa[4]+qa[5])/4.0;

}
