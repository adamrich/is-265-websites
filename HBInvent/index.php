<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>HB Branches</title>
        <script type="text/javascript" src="ajax.js"></script>
        <script type="text/javascript" src="Branch.js"></script>
    </head>
    <body>
        <h1>Henry Books Branch Inventory</h1>
        <p>Select a branch and click 'GO' to see inventory for that branch.</p>
        <br>
        <form id="BranchForm" action="BranchResults.php" method="get">
            <?php
                require_once('dbtest.php');
                
                $query = "SELECT * FROM branch ORDER BY Branch_Name;";
                $result = mysqli_query($dbc, $query);
                
                if(mysqli_num_rows($result) > 0) {
                    echo '<select id="branchid" name="branchid">';
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' .$row['Branch_Number']. '">'
                                .$row['Branch_Name']. '</option>';
                    }
                    echo '</select>';
                } else {
                    echo "<p>No Branches found!</p>";
                }
            ?>
            <input type="submit" name="go" id="go" value="Go" />
        </form>
        <div id="results"></div>
    </body>
</html>
