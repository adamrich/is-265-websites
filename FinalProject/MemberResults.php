<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Member History</title>
    </head>
    <body>
        <h1>Member Transaction History</h1>
        <?php
            $memberid = $_GET['memberid'];
            
            if(strlen($memberid) == 4) {
                require_once('dbtest.php');
                
                $query = "SELECT * FROM tblMembers WHERE MemID = '$memberid';";
                $result = mysqli_query($dbc, $query);
                $row = mysqli_fetch_array($result);
                if(mysqli_num_rows($result) > 0) {
                    echo "<p>Member ID: " .$row['MemID']. "<br>";
                    echo "Member Name: " .$row['LastName']. ", " .$row['FirstName']. " " .$row['MiddleName']. "<br>";
                    echo "Member Joined: " .$row['MemDt']. "<br>";
                    echo "Member Status: " .$row['Status']. "<br></p>";
                } else {
                    echo "<p>Member not on file.</p>";
                }
                
                //table for inventory
                echo "<table border='1'>";
                echo "<caption>Current Inventory</caption>";
                echo "<tr>";
                echo "<th>Purchase Date</th>";
                echo "<th>Trans Cd</th>";
                echo "<th>Trans Desc</th>";
                echo "<th>Trans Type</th>";
                echo "<th>Ammount</th>";
                echo "</tr>";
                
                $query2 = "SELECT p.Memid, p.PurchaseDt, p.TransCd, c.TransDesc, p.TransType, p.Amount
                            FROM tblPurchases p, tblCodes c
                            WHERE p.TransCd = c.TransCd AND p.MemId = '$memberid'
                            ORDER BY p.MemId, p.PurchaseDt, p.TransCd";
                $result2 = mysqli_query($dbc, $query2);
                
                while($row = mysqli_fetch_array($result2)) {
                    echo "<tr>";
                    echo "<td>" .$row['PurchaseDt']. "</td>";
                    echo "<td>" .$row['TransCd']. "</td>";
                    echo "<td>" .$row['TransDesc']. "</td>";
                    echo "<td>" .$row['TransType']. "</td>";
                    echo "<td align=right>$" .number_format($row['Amount'], $decimals=2). "</td>";
                    echo "</tr>";
                }
                echo "</table>";
            } else {
                echo '<p>No Member ID found.</p>';
            }
        ?>
    </body>
</html>
