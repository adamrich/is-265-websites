<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Club DB</title>
        <script type="text/javascript" src="ajax.js"></script>
        <script type="text/javascript" src="Member.js"></script>
    </head>
    <body>
        <h1>Club Member Purchase Review</h1>
        <p>Select a member and click 'GO' to see the transactions for that member.</p>
        <br>
        <form id="MemberForm" action="MemberResults.php" method="get">
            <?php
                require_once('dbtest.php');
                
                $query = "SELECT * FROM tblMembers ORDER BY LastName, FirstName, MiddleName;";
                $result = mysqli_query($dbc, $query);
                
                if(mysqli_num_rows($result) > 0) {
                    echo '<select id="memberid" name="memberid">';
                    while ($row = mysqli_fetch_array($result)) {
                        echo '<option value="' .$row['MemID']. '">'
                                .$row['LastName']. ", " .$row['FirstName']. " " .$row['MiddleName']. '</option>';
                    }
                    echo '</select>';
                } else {
                    echo "<p>No Members found!</p>";
                }
            ?>
            <input type="submit" name="go" id="go" value="Go" />
        </form>
        <div id="results"></div>
    </body>
</html>
