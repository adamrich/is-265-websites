"use strict";
var $ = function(id) { return document.getElementById(id); };

var displayCurrentTime = function() {
    var now = new Date();  //use the 'now' variable in all calculations, etc.
    let hour = now.getHours();
    let minute = now.getMinutes();
    let second = now.getSeconds();
    let AmPm = (hour<12) ? "AM" : "PM";

    if(minute < 10)
        minute = padSingleDigit(minute);
    if(second < 10)
        second = padSingleDigit(second);
    if(hour > 12)
        hour = hour - 12;
    else if(hour == 0)
        hour = 12;


    $("hours").innerHTML = hour;
    $("minutes").innerHTML = minute;
    $("seconds").innerHTML = second;
    $("ampm").innerHTML = AmPm;
};

var padSingleDigit = function(num) {
	if (num < 10) {	return "0" + num; }
	else { return num; }
};

window.onload = function() {
    // set initial clock display and then set interval timer to display
    // new time every second. Don't store timer object because it
    // won't be needed - clock will just run.
    displayCurrentTime();
    setInterval(displayCurrentTime, 1000);
};
